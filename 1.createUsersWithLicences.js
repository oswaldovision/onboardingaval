const newUsers = require('./newUsers.json')
const {getToken, createUser, assignLicences} = require('./graphHelper')
const secrets = require('./secrets')
const async = require('asyncawait/async');
const await = require('asyncawait/await');

let getNewToken = async (function(tenant){
  let token = await (getToken(tenant))

  let collection = []

  for (let i = 0; i < newUsers.length; i++) {
    console.log(`Current : ${newUsers[i].name} ${newUsers[i].lastName}`)

    let upn = `cl.${newUsers[i].name}.${newUsers[i].lastName}@${secrets.customDomain}`

    let body = {
      'accountEnabled': true,
      'displayName': `${newUsers[i].name} ${newUsers[i].lastName}`,
      'mailNickname': `${newUsers[i].name}`,
      'userPrincipalName': upn,
      'passwordProfile': {
        'forceChangePasswordNextSignIn': false,
        'password': secrets.initPassword
      },
      'showInAddressList': false,
      "usageLocation": "CO"
    }

    let userResponse = await (createUser(token, body))

    console.log(`User Created: upn: ${userResponse.userPrincipalName}, showInAddressList: ${userResponse.showInAddressList}`)

    let result = await (assignLicences(token,userResponse.id))

    console.log(`Result assignLicences of User: ${userResponse.userPrincipalName}, Result assignLicences: ${JSON.stringify(result)}`)

    collection.push(userResponse.userPrincipalName)
  }

  return newUsers.join(' ')
})


getNewToken(secrets.tenant).then(result => {
  console.log(result)
})










