const newUsers = require('./newUsers')
const { getTokenBearer, createEvent} = require('./graphHelper')
const secrets = require('./secrets')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

let execCreateEvent = async(function (tenant) {
  let arrayResult = []

  for (let i = 0; i < newUsers.length; i++) {
    console.log(`Current : ${newUsers[i].name} ${newUsers[i].lastName}`)

    let upn = `cl.${newUsers[i].name}.${newUsers[i].lastName}@${secrets.customDomain}`

    let tokenCurrentUser = await (getTokenBearer(tenant, upn, secrets.initPassword))

    let body = await (createEvent(tokenCurrentUser))

    if (body.id) {
      console.log(`Event created : ${newUsers[i].name} ${newUsers[i].lastName}`, body.id)
    }

    arrayResult.push(body.id)
  }

  return arrayResult
})

execCreateEvent(secrets.tenant).then(result => {
  console.log(result.join(','))
})