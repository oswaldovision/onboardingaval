const newUsers = require('./newUsers')
const { getTokenBearer, getFirstEmail,replyEmail} = require('./graphHelper')
const secrets = require('./secrets')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

let execGetEmail = async(function (tenant) {
  let arrayResult = []

  for (let i = 0; i < newUsers.length; i++) {
    console.log(`Current : ${newUsers[i].name} ${newUsers[i].lastName}`)

    let upn = `cl.${newUsers[i].name}.${newUsers[i].lastName}@${secrets.customDomain}`

    let tokenCurrentUser = await (getTokenBearer(tenant, upn, secrets.initPassword))

    let setEmails = await (getFirstEmail(tokenCurrentUser))

    if (JSON.parse(setEmails).value.length) {
      console.log(`Current : ${newUsers[i].name} ${newUsers[i].lastName}`, JSON.parse(setEmails).value[0].id)

      let replyEmails = await (replyEmail(tokenCurrentUser,JSON.parse(setEmails).value[0].id,secrets.commentReply))

      console.log(JSON.stringify(replyEmails))

    }

  }

  return arrayResult
})

execGetEmail(secrets.tenant).then(result => {
  console.log(result)
})