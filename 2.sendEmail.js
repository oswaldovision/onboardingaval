const newUsers = require('./newUsers')
const {sendEmail, getTokenBearer} = require('./graphHelper')
const secrets = require('./secrets')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

let execSendEmail = async(function (tenant) {
  let token = await(getTokenBearer(tenant, secrets.fromEmail, secrets.sendMail_password))

  let arrayResult = []

  for (let i = 0; i < newUsers.length; i++) {
    console.log(`Current : ${newUsers[i].name} ${newUsers[i].lastName}`)

    let upn = `cl.${newUsers[i].name}.${newUsers[i].lastName}@${secrets.customDomain}`

    let resultSendEmail = await(sendEmail(secrets.fromEmail, token, upn))

    arrayResult.push(`Enviado a:  ${upn}, resultado: ${JSON.stringify(resultSendEmail)}`)

  }

  return arrayResult
})

execSendEmail(secrets.tenant).then(result => {
  console.log(result)
})
