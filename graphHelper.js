const request = require('request')
const secrets = require('./secrets')

const helperGraph = {

  createEvent: function (token) {
    var options = {
      method: 'POST',
      url: 'https://graph.microsoft.com/v1.0/me/events',
      headers:
        {
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json',
          'Authorization': token
        },
      body:
        {
          subject: secrets.event_subject,
          body:
            {
              contentType: 'HTML',
              content: secrets.event_content
            },
          start:
            {
              dateTime: secrets.event_start,
              timeZone: secrets.event_zone
            },
          end:
            {
              dateTime: secrets.event_end,
              timeZone: secrets.event_zone
            },
          location: {displayName: secrets.event_location},
          attendees: []
        },
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  replyEmail: function (token, messageId, comment) {
    var options = {
      method: 'POST',
      url: `https://graph.microsoft.com/v1.0/me/messages/${messageId}/reply`,
      headers:
        {
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json',
          'Authorization': token
        },
      body: {comment: comment},
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(response)
        }
      })
    })
  },

  getFirstEmail: function (token) {
    let options = {
      method: 'GET',
      url: `https://graph.microsoft.com/beta/me/messages?subject eq ${secrets.sendMail_subjectFirstEmail}`,
      headers:
        {
          'Cache-Control': 'no-cache',
          'Authorization': token,
          'Content-Type': 'application/json'
        }
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  }
  ,

  assignLicences: function (token, userId) {
    let options = {
      method: 'POST',
      url: `https://graph.microsoft.com/beta/users/${userId}/assignLicense`,
      headers:
        {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
      body: {
        'addLicenses': [
          {
            'disabledPlans': [],
            'skuId': secrets.skuId
          }
        ],
        'removeLicenses': []
      },
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  }
  ,

  createUser: function (token, body) {
    let options = {
      method: 'POST',
      url: 'https://graph.microsoft.com/beta/users/',
      headers:
        {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
      body: body,
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  }
  ,

  getToken: function (tenantId) {
    let options = {
      method: 'POST',
      url: `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      headers:
        {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      form:
        {
          client_id: secrets.client_id,
          scope: secrets.scope,
          client_secret: secrets.client_secret,
          grant_type: secrets.grant_type
        }
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.parse(body).access_token)
        }
      })
    })
  }
  ,

  getTokenBearer: function (tenantId, username, password) {
    var options = {
      method: 'POST',
      url: `https://login.microsoftonline.com/${tenantId}/oauth2/token`,
      headers:
        {
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      form:
        {
          client_id: secrets.client_id,
          client_secret: secrets.client_secret,
          username: username,
          password: password,
          grant_type: secrets.sendMail_grant_type,
          resource: secrets.sendMail_resource,
          scope: secrets.sendMail_scope
        }
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.parse(body).access_token)
        }
      })
    })
  }
  ,

  sendEmail: function (from, token, to) {
    let options = {
      method: 'POST',
      url: `https://graph.microsoft.com/v1.0/me/sendMail`,
      headers:
        {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
      body: {
        'message': {
          'subject': secrets.sendMail_subjectFirstEmail,
          'body': {
            'contentType': 'Text',
            'content': secrets.commentwelcomeEmail
          },
          'toRecipients': [
            {
              'emailAddress': {
                'address': to
              }
            }
          ]
        }
      },
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(response)
        }
      })
    })
  }
}

module.exports = helperGraph
